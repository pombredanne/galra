#!/usr/bin/env python
from flask import Flask
from flask import request
from flask import render_template
import requests

import logging

logger = logging.getLogger(__name__)

# app = Flask(__name__)
app = Flask(__name__, template_folder="http")

@app.route('/<distro>/<release>/preseed')
@app.route('/<distro>/preseed', defaults={'release': None})
def get_preseed(distro, release):
    path = get_path(distro, "preseed.cfg", release=release)
    kwargs = {}
    username = request.args.get('password', 'vagrant')
    password = request.args.get('username', 'vagrant')
    proxy = request.args.get('proxy', "")
    pubkey = get_pubkey(request)

    if not request.args.get('nouser', None):
        kwargs = {
            "username": username,
            "password": password,
            "pubkey": pubkey,
            "proxy": proxy,
        }

    if request.args.get('vmware', None) is None:
        kwargs['vmware'] = False
    else:
        kwargs['vmware'] = True

    content = render_template(path, **kwargs)
    return content

@app.route('/<family>/vmware_tools')
def get_vmware_tools(family):
    logger.info("Received request for vmware_tools")
    kwargs = {
        "workdir": request.args.get('workdir', '/home/vagrant/work'),
        "mountdir": request.args.get('mountdir', '/mnt/vmware'),
        "iso": request.args.get('iso', '/home/vagrant/linux.iso'),
    }

    path = get_path(family, "vmware_tools.sh")

    content = render_template(path, **kwargs)
    return content

@app.route('/<family>/post_install')
def get_post_install(family):
    logger.info("Received request for post_install")
    username = request.args.get('username', 'vagrant')
    workdir = request.args.get('workdir', '/home/{0}/work'.format(username))
    release = request.args.get('release', 'precise')

    pubkey = get_pubkey(request)
    path = get_path(family, "post_install.sh")

    content = render_template(path, username=username,
                                    pubkey=pubkey,
                                    release=release,
                                    workdir=workdir)
    return content

@app.route('/<family>/mininet_install')
def get_mininet_install(family):
    logger.info("Received request for mininet_install")
    username = request.args.get('username', 'vagrant')
    workdir = request.args.get('workdir', '/home/{0}/work'.format(username))

    path = get_path(family, "mininet_install.sh")
    kwargs = {
        'username': username,
        'workdir': workdir,
    }

    content = render_template(path, **kwargs)
    return content

def get_pubkey(request):
    vagrant_pubkey = 'https://raw.github.com/mitchellh'
    vagrant_pubkey += '/vagrant/master/keys/vagrant.pub'
    r = requests.get(vagrant_pubkey)
    pubkey = request.args.get('pubkey', r.text)
    return pubkey

def get_path(distro, filename, release=None):
    if release is not None:
        path = "{distro}/{release}/{filename}".format(distro=distro,
                                                      release=release,
                                                      filename=filename)
    else:
        path = "{distro}/{filename}".format(distro=distro, filename=filename)

    return path

@app.before_first_request
def setup_logging():
    app.logger.addHandler(logging.StreamHandler())
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run('0.0.0.0', debug=True)
