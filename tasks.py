import os
import shutil
import subprocess
import signal
import sys
import select
from invoke import task, run, exceptions

@task
def makedirs():
    """
    Creates temporary directories like tmp and output.
    """
    if not os.path.exists("output/boxes"):
        os.makedirs("output/boxes")
    if not os.path.exists("tmp"):
        os.makedirs("tmp")

@task
def clean_cache():
    """
    Cleans out the packer cache files.
    """
    shutil.rmtree("packer_cache")

@task("makedirs")
def server():
    """
    Starts the file server.
    """
    pid = None
    pid_path = "tmp/packer_gunicorn.pid"
    if os.path.exists(pid_path):
        with open(pid_path) as f:
            pid = int(f.read().strip())

    if pid is not None:
        try:
            os.kill(pid, 0)
            print("Server is running...")
        except OSError as e:
            print("Starting File Server...")
            subprocess.check_call(['gunicorn', '-c', 'config/gunicorn.py', 'server:app'])
    else:
        print("Starting File Server...")
        subprocess.check_call(['gunicorn', '-c', 'config/gunicorn.py', 'server:app'])


@task
def killserver():
    """
    Kill the file server.
    """
    with open('tmp/packer_gunicorn.pid') as f:
        pid = f.read().strip()
    os.kill(int(pid), signal.SIGTERM)

@task
def build(template, headless=True, proxy="", host="", only=""):
    """
    Build an image target defined in 'template'.
    """
    server()
    # from stackoverflow http://tinyurl.com/cy67pgm
    cmd = ['packer', 'build', '--force']

    if not headless:
        cmd.extend(["-var", "headless=false"])

    if proxy:
        cmd.extend(["-var", "proxy={0}".format(proxy)])

    if host:
        cmd.extend(["-var", 'host={0}'.format(host)])

    if only:
        cmd.extend(["-only", only])

    cmd.append(template)

    run(" ".join(cmd))

@task
def vagrant(name):
    remove = ["vagrant", "box", "remove", "{0}-x86_64".format(name)]
    try:
        run(" ".join(remove))
    except exceptions.Failure:
        pass

    add = ["vagrant", "box", "add",
           "{0}-x86_64".format(name),
           "output/boxes/{0}-x86_64_vmware.box".format(name)]
    run(" ".join(add))