#!/usr/bin/env python
from __future__ import print_function
import os
import sys

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

def checkmsg(name):
	msg = bcolors.OKBLUE + "Checking for " + name + ": " + bcolors.ENDC
	print(msg, end="")

def message(found):
	if found:
		print(bcolors.OKGREEN + "Found!" + bcolors.ENDC)
	else:
		msg = bcolors.FAIL + "Not found!" + bcolors.ENDC
		print(msg)

def check(name):
	found = False
	checkmsg(name)
	for p in os.environ['PATH'].split(":"):
		if os.path.exists(os.path.join(p, name)):
			found = True
	message(found)

def pycheck(name):
	checkmsg(name)
	try:
		__import__(name)
		found = True
	except ImportError as e:
		print(e)
		found = False
	message(found)

def main():
	check("packer")

	with open('requirements.txt') as f:
		for mod in f.readlines():
			pycheck(mod.strip())


if __name__ == "__main__":
	main()
