#!/bin/bash
set -ex
cd {{ workdir }}

# Mount setup file
mkdir -p {{ mountdir }}
mount -o loop {{ iso }} {{ mountdir }}

# Extract setup setup
tar xzf /mnt/vmware/VMwareTools-*.tar.gz

# Unmount setup file
umount {{ mountdir }}

# Install Dependencies
apt-get install  psmisc gcc make linux-headers-`uname -r` -y

# Execute setup
{{ workdir }}/vmware-tools-distrib/vmware-install.pl --default
echo vmhgfs >> /etc/modules
echo vmxnet >> /etc/modules

# Clean
rm -fr {{ workdir }}/vmware-tools-distrib

apt-get purge  psmisc gcc make linux-headers-`uname -r` -y
apt-get autoremove --purge -y
