#!/bin/bash
set -ex

function setup() {
  mkdir -p {{ workdir }}
}

function disable_cdrom_source() {
  sed -i 's/^deb cdrom/# DISABLED deb cdrom/' /etc/apt/sources.list
}

function configure_user() {
  echo '{{ username }} ALL = (ALL) NOPASSWD: ALL' > /etc/sudoers.d/99-{{ username }}
  chmod 440 /etc/sudoers.d/99-{{ username }}
  mkdir -p /home/{{ username }}/.ssh
  chmod 700 /home/{{ username }}/.ssh
  echo "{{ pubkey }}" >> /home/{{ username }}/.ssh/authorized_keys
  chmod 600 /home/{{ username }}/.ssh/authorized_keys
  chown -R {{ username }}:{{ username }} /home/{{ username }}/.ssh
}

function install_puppet() {
  cd {{ workdir }}
  wget --no-check-certificate http://apt.puppetlabs.com/puppetlabs-release-{{ release }}.deb \
    -O {{ workdir }}/puppetlabs-release-{{ release }}.deb
  dpkg -i puppetlabs-release-{{ release }}.deb
  apt-get update
  apt-get install puppet -y
}

function upgrade() {
  apt-get update
  apt-get dist-upgrade -y
}

function cleanup() {
  aptitude -y purge ri
  aptitude -y purge installation-report wireless-tools wpasupplicant
  aptitude -y purge python-dbus libnl1 python-smartpm python-twisted-core
  aptitude -y purge python-twisted-bin libdbus-glib-1-2 python-pexpect
  aptitude -y purge python-pycurl python-serial python-gobject python-pam
  aptitude -y purge python-openssl libffi5 libiw30

  apt-get clean -y
  apt-get autoclean -y

  # Remove bash history
  unset HISTFILE
  rm -f /root/.bash_history
  rm -f /home/vagrant/.bash_history

  # Cleanup log files
  find /var/log -type f | while read f; do echo -ne '' > $f; done;
}

function main() {
  setup
  disable_cdrom_source
  upgrade
  install_puppet
  configure_user
  cleanup
  reboot
  # pause for enough time for reboot to take effect
  sleep 30
}

main
