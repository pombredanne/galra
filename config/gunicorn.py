import os

bind = "0.0.0.0:5000"
workers = 4
worker_class = "sync"
debug = True
pidfile = "tmp/packer_gunicorn.pid"
accesslog = "tmp/gunicorn.log"
errorlog = "tmp/gunicorn.log"
loglevel = "debug"
daemon = True